#pragma once
#include "Talia.h"
class Wyglad : public Talia
{
public:
	Wyglad();

	void Skalowanie(int Szer, int Wys);
	void EkranGry();
	void UsuwanieKarty(int x, int y);
	void WypelnianieKarty(int x, int y, int wartosc, string znak, string kolor);
	void WydajZPozosta�ych(int wartosc, string znak, string kolor);
	void ZaznaczKarte(int x, int y);
	void OdznaczKarte(int x, int y);
	void Wygrana();
	void Start();
	~Wyglad();
};

